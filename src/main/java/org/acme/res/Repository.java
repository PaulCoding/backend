package org.acme.res;


import org.acme.entities.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Repository {
    @Inject
    private EntityManager em;


    public Repository(EntityManager entityManager)
    {
        em = entityManager;
    }

    public List<RoomMessage> getRoomMessages(long roomID)
    {
        var msg =  em.createQuery("select m from RoomMessage m where m.RoomID=:ID", RoomMessage.class);
        msg.setParameter("ID",roomID);

        return msg.getResultList();
    }

    public List<Member> getAllPersonsOffGroup(long roomID)
    {
        var query =  em.createQuery("Select p from  Member p where p=:roomID", Member.class);
        query.setParameter("roomID", roomID);

        return query.getResultList();
    }


    public void UpdateImage(int UserID, ImageUpdate imageUpdate)
    {
        var person = getPerson(UserID);
        person.setProfileImage(imageUpdate.getImageUpdate());
    }


    public Room getRoomByName(String name)
    {
        var q = em.createQuery("Select r from Room r where r.Name=:name", Room.class);
        q.setParameter("name", name);
        return q.getResultStream().findFirst().orElse(null);
    }


    public boolean checkIfUserIsJoined(Person person, Room room)
    {
        var q = em.createQuery("select m from Member m where m.User.id=:user and  m.Room.id=:room",Member.class);
        q.setParameter("user", person.id);
        q.setParameter("room", room.id);

        if(q.getResultStream().findFirst().orElse(null) != null) return true;
        return false;
    }
    public void addPersonToGroup(Person person, Room roomID)
    {
        var room = getRoomByName(roomID.getName());
//        var members = getAllPersonsOffGroup(roomID.id);

        if(room != null && !checkIfUserIsJoined(person, room))
        {
            Member member = new Member();
            member.setRoom(room);
            member.setUser(person);
            em.persist(member);
        }
    }

    public List<Person> getAllPersons()
    {
        return em.createQuery("Select p from Person p", Person.class).getResultList();
    }


    public List<Member> getMembershipsOfUser(long user)
    {
        var membership = em.createQuery("select  m from Member m where m.User=:user",Member.class);
        membership.setParameter("user", getPerson(user));
        return membership.getResultList();
    }

    public Person getPerson(long id)
    {
        var person =  em.createQuery("select p from Person p where p.id=:ID", Person.class);
        person.setParameter("ID",id);
        return person.getResultStream().findFirst().orElse(null);
    }

    public List<Message> getNormalMessage(String senderName, String recieverName)
    {
        List<Message> returnV = new ArrayList<>();
        List<Message> messages = Message.listAll();
        System.out.println(messages.size());
        for (Message msg: messages) {

            if((msg.getRecieverID().equals(senderName) || msg.getSenderID().equals(senderName) )&&( (msg.getRecieverID().equals(recieverName) || msg.getSenderID().equals(recieverName))))
            {
                returnV.add(msg);
            }
        }

        return returnV;
    }

    public Room createRoom(Room room) {
        em.persist(room);

        return room;
    }

    public Object getRoomByByID(long roomID) {
        var q = em.createQuery("Select r from Room r where r.id=:id", Room.class);
        q.setParameter("id", roomID);
        return q.getResultStream().findFirst().orElse(null);
    }

    public Person getPersonByName(String username) {
        var person =  em.createQuery("select p from Person p where p.Name=:ID", Person.class);
        person.setParameter("ID", username);
        return person.getResultStream().findFirst().orElse(null);
    }

    public void leaveGroup(Person person, Room room) {
        var newRoom = getRoomByName(room.getName());
//        var members = getAllPersonsOffGroup(roomID.id);
        System.out.println(room.getName());
        if(newRoom != null && checkIfUserIsJoined(person, newRoom))
        {
            var query = em.createQuery("select m from Member m where m.Room=:r and m.User=:p", Member.class);
            query.setParameter("r", room);
            query.setParameter("p",person);
            Member member = query.getResultStream().findFirst().orElse(null);
            System.out.println(member.id);
            System.out.println("GOT HERE");
            em.remove(member);
        }
    }
}
