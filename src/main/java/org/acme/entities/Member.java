package org.acme.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Member extends PanacheEntity {

    @ManyToOne
    Person User;

    @ManyToOne
    Room Room;

    public void setUser(Person user) {
        User = user;
    }

    public Person getUser() {
        return User;
    }

    public Room getRoom() {
        return Room;
    }

    public void setRoom(Room room) {
        Room = room;
    }

    @Override
    public String toString() {
        return "Member{" +
                "UserID=" + User +
                ", RoomID=" + Room +
                ", id=" + id +
                '}';
    }
}
