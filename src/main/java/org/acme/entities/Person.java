package org.acme.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Person extends PanacheEntity {

    String Name;
    String Password;
    String ProfileImage = "https://image.shutterstock.com/image-illustration/male-default-placeholder-avatar-profile-260nw-582509551.jpg";



    public Person() {
    }

    public Person(String name, String password, String profileImage) {
        Name = name;
        Password = password;
        ProfileImage = profileImage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }


    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Name='" + Name + '\'' +
                ", Password='" + Password + '\'' +
                ", ProfileImage='" + ProfileImage + '\'' +
                ", id=" + id +
                '}';
    }


    @OneToMany(cascade = {CascadeType.ALL})
    List<Member> members = new ArrayList<>();


    public void setMembers(List<Member> members) {
        this.members = members;
    }
}
