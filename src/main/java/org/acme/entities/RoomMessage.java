package org.acme.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class RoomMessage extends PanacheEntity {
    long RoomID;
    String senderID;
    String message;
    Date date;

    public RoomMessage() {
    }


    public RoomMessage(long roomID, String senderID, String message, Date date) {
        RoomID = roomID;
        this.senderID = senderID;
        this.message = message;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getRoomID() {
        return RoomID;
    }

    public void setRoomID(long roomID) {
        RoomID = roomID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String msg) {
        this.message = msg;
    }
}
