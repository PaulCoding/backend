package org.acme.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.*;

import javax.persistence.Entity;
import java.util.Date;

@Entity

@NoArgsConstructor
@RequiredArgsConstructor
public class Log extends PanacheEntity {
    @NonNull private String message;
    @NonNull private Date createdAt;


}
