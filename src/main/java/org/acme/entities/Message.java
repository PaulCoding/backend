package org.acme.entities;


import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Date;

@Entity
public class Message extends PanacheEntity {
    private String message;
    private String senderID;
    private String recieverID;
    private Date date;

    public Message() {
    }

    public Message(String message, String senderID, String recieverID, Date date) {
        this.message = message;
        this.senderID = senderID;
        this.recieverID = recieverID;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getRecieverID() {
        return recieverID;
    }

    public void setRecieverID(String recieverID) {
        this.recieverID = recieverID;
    }
}
