package org.acme.entities;

public class ImageUpdate {
    String imageUpdate;

    public ImageUpdate() {
    }

    public ImageUpdate(String imageUpdate) {
        this.imageUpdate = imageUpdate;
    }

    public String getImageUpdate() {
        return imageUpdate;
    }

    public void setImageUpdate(String imageUpdate) {
        this.imageUpdate = imageUpdate;
    }
}
