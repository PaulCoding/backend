package org.acme.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Room extends PanacheEntity {

    String Name;

    @OneToMany(cascade = {CascadeType.ALL})
    List<Member> members = new ArrayList<>();
    public String getName() {
        return Name;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public void setName(String name) {
        Name = name;
    }


    @Override
    public String toString() {
        return "Room{" +
                "Name='" + Name + '\'' +
                ", id=" + id +
                '}';
    }
}
