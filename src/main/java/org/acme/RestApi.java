package org.acme;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.acme.entities.*;
import org.acme.res.Repository;
import org.acme.security.Roles;
import org.acme.security.TokenService;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.awt.*;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/chat/rest")
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RestApi {


    @Inject
    TokenService service;

    @Context
    SecurityContext securityContext;

    private final Repository repository;

    public RestApi(Repository repository)
    {
        this.repository = repository;
    }


    @GET()
    @Path("permit-all")
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@Context SecurityContext ctx) {
        return getResponseString(ctx);
    }

    private String getResponseString(SecurityContext ctx) {
        return service.generateUserToken("1paulengelhardt@gmail.com", "test1");
    }


    @POST
    @Path("login")
    @Transactional
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public Response login(AuthRequest auth) {
        System.out.println(auth.password);
        Person user= repository.getPersonByName(auth.username);
        if(user != null)
        {
            if(user.getPassword().equals(auth.getPassword()))
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("token", new JsonPrimitive(service.generateUserToken("example@gmail.com", auth.password)));
                return Response.ok(jsonObject.toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }


    @GET
    @Path("live")
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Produces(MediaType.TEXT_PLAIN)
    public String GetMessages(){
        return "test";
    }


    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("UserByName/{username}")
    public Response GetUser(@PathParam("username") String username)
    {
        return Response.ok(repository.getPersonByName(username)).build();
    }
    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("Room/{roomID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetMessages(@PathParam("roomID") long roomID){
        return Response.ok(repository.getRoomMessages(roomID)).build();
    }

    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("RoomInfo/{roomID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetRoom(@PathParam("roomID") long roomID){
        return Response.ok(repository.getRoomByByID(roomID)).build();
    }

    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("User/getID/{ID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("ID") long id)
    {
        return Response.ok(repository.getPerson(id)).build();
    }

    @POST
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("Room/create/Create")
    @Transactional
    public Response CreateRoom(Room room)
    {
        if(repository.getRoomByName(room.getName()) == null) {
            return Response.ok(repository.createRoom(room)).build();
        }
        return (false ? Response.ok() : Response.status(Response.Status.BAD_REQUEST)).build();
    }

    @POST
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Room/{userID}")
    public void joinRoom(@PathParam("userID")long user, Room room)
    {
        repository.addPersonToGroup(repository.getPerson(user), room);
    }


    @POST
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Path("LeaveRoom/{userID}")
    public Response leaceRoom(@PathParam("userID")long user, Room room)
    {
        repository.leaveGroup(repository.getPerson(user), room);
        return Response.ok().build();
    }


    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("Person/rooms/{membership}")
    public  Response getRooms(@PathParam("membership") long userID)
    {
        return  Response.ok(repository.getMembershipsOfUser(userID)).build();
    }
    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("User/{userID}/{otheruserID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetMessages(@PathParam("userID") String senderName, @PathParam("otheruserID") String recieverName){
        return Response.ok(repository.getNormalMessage(senderName, recieverName)).build();
    }


    @Path("ImageUpdate/{id}")
    @POST
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response UpdateImage(@PathParam("id") int id, ImageUpdate image)
    {
        System.out.println("Goth ehere: "+image.getImageUpdate());
        repository.UpdateImage(id, image);
        return (true ? Response.ok() : Response.status(Response.Status.BAD_REQUEST)).build();
    }
    @GET
    @RolesAllowed({Roles.USER, Roles.SERVICE})
    @Path("User/allUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers(){
        return Response.ok(repository.getAllPersons()).build();
    }
}
