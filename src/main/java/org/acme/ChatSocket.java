package org.acme;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.transaction.*;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.acme.entities.*;
import org.acme.res.Repository;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;
import org.graalvm.nativeimage.Platform;
import org.jboss.logging.Logger;

@ServerEndpoint("/chat/{username}/{password}")
@ApplicationScoped
public class ChatSocket {

    private static final Logger LOG = Logger.getLogger(ChatSocket.class);
    private final Repository repository;

    public ChatSocket(Repository repository)
    {
        this.repository = repository;
    }




    ManagedExecutor executor = ManagedExecutor.builder()
            .maxAsync(5)
            .propagated(ThreadContext.CDI,
                    ThreadContext.TRANSACTION)
            .build();
    ThreadContext threadContext = ThreadContext.builder()
            .propagated(ThreadContext.CDI,
                    ThreadContext.TRANSACTION)
            .build();

    @Inject
    UserTransaction userTransaction;
    Map<String, Session> sessions = new ConcurrentHashMap<>();



/*
    @GET
    @Path("Room/{roomID}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RoomMessage> GetMessages(@PathParam("roomID") int roomiID){
        return repository.getRoomMessages(roomiID);
    }



    @GET
    @Path("User/{userID}/{otheruserID}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> GetMessages(@PathParam("userID") String senderName,@PathParam("otheruserID") String recieverName){
        return repository.getNormalMessage(senderName, recieverName);
    }


    @GET
    @Path("User/allUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getAllUsers(){
        return repository.getAllPersons();
    }


*/

    @OnOpen
    @Path("chatting/{username}/{password}")
    public void onOpen(Session session, @PathParam("username") String username, @PathParam("password") String password) {
        executor.runAsync(threadContext.contextualRunnable(() -> {
            try {
                userTransaction.begin();

                Person user= CheckIfUserExists(username);
                if(user == null)
                {
                    user = new Person(username, password, "https://image.shutterstock.com/image-illustration/male-default-placeholder-avatar-profile-260nw-582509551.jpg");
                    System.out.println("new User created");
                    user.persist();
                }
                else if(!user.getPassword().equals(password)) {

                    session.close();
                    return;
                }
                System.out.println("new Login");
                System.out.println(user);
                sessions.put(username, session);
                System.out.println("====================================================");
                System.out.println(user.id);
                System.out.println(sessions.size());
                System.out.println("====================================================");
                userTransaction.commit();

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("type",new JsonPrimitive("info"));
                jsonObject.add("message", new JsonPrimitive("joined"));
                jsonObject.add("Username", new JsonPrimitive(username));
                jsonObject.add("Image", new JsonPrimitive(user.getProfileImage()));
                broadcast(jsonObject, username);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }));
    }

    @OnClose
    public void onClose(Session session, @PathParam("username") String username,  @PathParam("password") String password) {
        sessions.remove(username);
    //    broadcast("info","User " + username + " left", username);
    }

    @OnError
    public void onError(Session session, @PathParam("username") String username, @PathParam("password") String password, Throwable throwable) {
        sessions.remove(username);
        LOG.error("onError", throwable);
   //     broadcast("info", "User " + username + " left on error: " + throwable, username);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("username") String username, @PathParam("password") String password) {
        SimpleDateFormat format = new SimpleDateFormat("HH;mm;ss yyyy-MM-dd");
    //    String message = sendMSG.substring(1, sendMSG.length() - 1);
        message = message.replace("\\\"", "\"");
        message = message.substring(1, message.length() - 1);
        System.out.println(message);
        JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();
        String type = jsonObject.getAsJsonPrimitive("type").toString().substring(1, jsonObject.getAsJsonPrimitive("type").toString().length() - 1);;
        System.out.println(type);
        executor.runAsync(threadContext.contextualRunnable(() -> {
            try {

            userTransaction.begin();
            if(CheckIfUserExists(username) != null) {
                if (false) {
                    //broadcast("info", message, username);
                } else {
                    if (type.equals("msg")) {
                        System.out.println(jsonObject.getAsJsonPrimitive("target").toString());
                        Person user = CheckIfUserExists(jsonObject.getAsJsonPrimitive("target").toString().substring(1, jsonObject.getAsJsonPrimitive("target").toString().length() - 1));
                        Person sender = CheckIfUserExists(username);
                        Message msg = new Message(jsonObject.get("msg").toString(), sender.getName(),user.getName(), new Date());
                        msg.persist();
                        System.out.println(user);
                        if (user != null) {

                            JsonObject retObj = new JsonObject();
                            retObj.add("type", new JsonPrimitive("msg"));
                            retObj.add("Sender", new JsonPrimitive(username));
                            retObj.add("date", new JsonPrimitive('"'+format.format(msg.getDate())+'"'));
                            retObj.add("msg", new JsonPrimitive(jsonObject.get("msg").toString()));
                            broadcast(retObj, user.getName());
                        }
                    }
                    else if(type.equals("groupmsg"))
                    {
                        System.out.println(jsonObject);
                        Person sender = CheckIfUserExists(username);
                       // List<Message> messages = Message.listAll();
                        String target = jsonObject.getAsJsonPrimitive("target").toString();
                        int targetGroup = Integer.parseInt(target.substring(1, target.length() - 1));
                        System.out.println(targetGroup);
                        RoomMessage msg = new RoomMessage(targetGroup, sender.getName(), jsonObject.get("msg").toString(), new Date());
                        msg.persist();
                        List<Member> members = Member.listAll();


                        JsonObject retObj = new JsonObject();
                        retObj.add("type", new JsonPrimitive("groupmsg"));
                        retObj.add("Group", new JsonPrimitive(msg.getRoomID()));
                        retObj.add("Sender", new JsonPrimitive(sender.getName()));
                        retObj.add("msg", new JsonPrimitive(msg.getMessage()));
                        retObj.add("date", new JsonPrimitive('"'+format.format(msg.getDate())+'"'));

                        for (var member: members) {
                            if(!member.getUser().getName().equals(username) && member.getRoom().id == msg.getRoomID())
                            {
                                System.out.println("Send to member "+member.getUser().getName() + " "+username);
                                broadcast(retObj, member.getUser().getName());
                            }
                        }



                    }
                }
            }


            userTransaction.commit();
            } catch (RollbackException e) {
                e.printStackTrace();
            } catch (HeuristicMixedException e) {
                e.printStackTrace();
            } catch (HeuristicRollbackException e) {
                e.printStackTrace();
            } catch (SystemException e) {
                e.printStackTrace();
            } catch (NotSupportedException e) {
                e.printStackTrace();
            }

        }));
    }

    Person CheckIfUserExists(String name)
    {
        List<Person> allUser = Person.listAll();

        for (Person user:allUser) {
            if(user.getName().equals(name)) {
                System.out.println(user);
                return user;
            }
        }

        return null;
    }

    private void broadcast(JsonObject ob, String target) {
        SimpleDateFormat format = new SimpleDateFormat("HH;mm;ss yyyy-MM-dd");

        Session session = sessions.get(target);
        System.out.println(session);

        executor.runAsync(threadContext.contextualRunnable(() -> {
            try {
                sessions.get(target).getAsyncRemote().sendObject(ob.toString(),result -> {
                    if (result.getException() != null) {
                        System.out.println("Unable to send message: " + result.getException());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

/*        if (type.equals("info")) {
            executor.runAsync(threadContext.contextualRunnable(() -> {
                try {
                    userTransaction.begin();
                    sessions.get(target).getAsyncRemote().sendObject(type+":"+message,result -> {
                        if (result.getException() != null) {
                            System.out.println("Unable to send message: " + result.getException());
                        }
                    });
                    userTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));

        } else if( type.equals("msg")){
            executor.runAsync(threadContext.contextualRunnable(() -> {
                try {
                    userTransaction.begin();
                    String[] data = message.split(":");
                    Message msg = new Message(data[1], data[0], target, new Date());
                    msg.persist();
                    sessions.get(target).getAsyncRemote().sendObject("msg:"+format.format(msg.getDate())+":"+data[0]+":"+target+":"+data[1],result -> {
                        if (result.getException() != null) {
                            System.out.println("Unable to send message: " + result.getException());
                        }
                    });
                    userTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        }
        else if(type.equals("allmsg"))
        {
            executor.runAsync(threadContext.contextualRunnable(() -> {
                try {
                    userTransaction.begin();
                    sessions.get(target).getAsyncRemote().sendObject(message,result -> {
                        if (result.getException() != null) {
                            System.out.println("Unable to send message: " + result.getException());
                        }
                    });
                    userTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        }

 */
    }



}